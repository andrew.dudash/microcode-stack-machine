module test_cpu();
   reg clock;
   reg reset;   
   cpu CPU(clock, reset);

   initial begin
      clock <= 1'b0;
      reset <= 1'b1;
      #10;      
      #10 clock <= 1'b1;
      #10 clock <= 1'b0;
      #10 clock <= 1'b1;
      #10 clock <= 1'b0;
      reset <= 1'b0;
      #10;
      

      for (int i = 0; i < 10; i = i + 1) begin
	 clock <= 1'b0;
	 #10;	 
	 clock <= 1'b1;
	 #10;	 
      end
   end
endmodule
