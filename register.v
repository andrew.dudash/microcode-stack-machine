module register(bus, write, read, clock, reset);
   inout [15:0] bus;
   input 	write;
   input 	read;   
   input 	clock;
   input 	reset;
   reg [15:0] 	memory;

   assign bus = read ? memory : 16'bz;

   output [15:0] direct;
   assign direct = memory;   
   
   always @ (posedge clock)
     if (reset)
       memory <= 16'b0;
     else
       if (write)
	 memory <= bus;
endmodule
	
   
