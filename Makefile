test: test_alu.out test_register.out test_micro_cpu.out test_cpu.out

test_cpu.out: test_cpu.v cpu.v register.v micro_cpu.v
	iverilog -g 2009 -o test_cpu.out test_cpu.v cpu.v register.v micro_cpu.v alu.v

test_alu.out: test_alu.v alu.v
	iverilog -g 2009 -o test_alu.out test_alu.v alu.v

test_register.out: test_register.v register.v
	iverilog -g 2009 -o test_register.out test_register.v register.v

test_micro_cpu.out: test_micro_cpu.v micro_cpu.v
	iverilog -g 2009 -o test_micro_cpu.out test_micro_cpu.v micro_cpu.v
