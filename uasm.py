import struct

def make_uop(source=None, destination=None, data_index_adjust=None, alu_op=None, done=False):
    register_table = {
        'A': 0,
        'B': 1,
        'STACK': 2,
        'ALU': 3,
        None: 3
    }
    source_code = register_table[source]
    destination_code = register_table[destination]
    data_index_adjust_code = {
        '+': 0,
        '-': 1,
        None: 2
    }[data_index_adjust]
    alu_op_code = {
        '+': 0,
        '-': 1,
        'LOAD': 2,
        None: 3
    }[alu_op]
    done_code = {
        True: 1,
        False: 0
    }[done]
    
    u_op = done_code
    u_op = u_op | (source_code << 1)
    u_op = u_op | (destination_code << 4)
    u_op = u_op | (alu_op_code << 7)
    u_op = u_op | (data_index_adjust_code << 9)
    return hex(u_op)

ROM_SIZE = 256
BLOCK_COUNT = 16
BLOCK_SIZE = ROM_SIZE / BLOCK_COUNT
NULL_UOP = make_uop()
def dump_uop_rom(blocks, destination):
    rom_buffer = [None for _ in range(U_ROM_SIZE)]
    for block_index, block in enumerate(blocks):
        for uop_index, uop in enumerate(block):
            rom_buffer[(block_index * BLOCK_SIZE) + uop_index] = uop
    for uop in rom_buffer:
        if not uop:
            uop = NULL_UOP
        destination.write('{}\n'.format(uop))

with open('micro-rom.hex', 'w') as F_OBJ:
    dump_uop_rom(
        blocks=[
            # DUP
            [
                make_uop(
                    source='STACK',
                    destination='A',
                    data_index_adjust='+'
                ),
                make_uop(
                    source='A',
                    destination='STACK',
                    done=True
                )
            ],
            # ADD
            [
                make_uop(
                    source='STACK',
                    data_index_adjust='-',
                    alu_op='LOAD'
                ),
                make_uop(
                    source='STACK',
                    destination='A'
                ),
                make_uop(
                    source='ALU',
                    destination='STACK',
                    done=True
                )
            ]
        ],
        destination=F_OBJ
    )
        
        
    
