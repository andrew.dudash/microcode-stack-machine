module cpu(clock, reset);
   reg [15:0] instruction_index;
   reg [15:0] instructions [255:0];
   wire [15:0] current_instruction;   
   assign current_instruction = instructions[instruction_index];   

   reg [15:0] data_index;
   reg [15:0] data_stack [255:0];

   wire [15:0] bus;

   wire 	      write_a;
   wire 	      write_b;
   wire 	      write_accum;

   wire 	      read_a;
   wire 	      read_b;
   wire 	      read_accum;
   wire 	      read_alu;   

   input 	      clock;
   input 	      reset;

   wire [1:0] 	      alu_op;
   wire [15:0] 	      alu_result;
   wire [1:0] 	      stack_index_adjust_code;
   wire 	      micro_instruction_done;
   
   
   
   

   register REGISTER_A(bus, write_a, read_a, clock, reset);
   register REGISTER_B(bus, write_b, read_b, clock, reset);
   alu ALU(bus, alu_op, alu_result, read_alu, clock, reset);

   wire [1:0] 	      data_stack_index_adjust_code;   

   micro_cpu MICRO(
	     .instruction(current_instruction),
	     .read_a(read_a),
	     .read_b(read_b),
	     .read_stack(read_stack),
	   .read_alu(read_alu),
	     .write_a(write_a),
	     .write_b(write_b),
	     .write_stack(write_stack),
	     .stack_index_adjust_code(stack_index_adjust_code),
	     .alu_op(alu_op),
	     .clock(clock),
	     .done(done),
	     .reset(reset)
	     );
   
   initial begin
      //$readmemh("rom.hex", instructions);
      instructions[0] = 16'd0;
      instructions[1] = 16'd1;
   end

   always @ (posedge clock) begin
      $display("Data Stack");      
      for (int i = 4; i >= 0; i = i - 1) begin
	 $display("%d: 0x%x", i, data_stack[i]);
      end
      $display("Bus: 0x%x", bus);
      $display("Data Index: %d", data_index);
      $display("");      
   end

   always @ (posedge clock)
     $display("Read A: %d, Read B: %d, Read Stack: %d, Write A: %d, Write B: %d, Write Stack: %d, Stack Index Adjust Code: %d, ALU Op: %d, Done %d", read_a, read_b, read_stack, write_a, write_b, write_stack, stack_index_adjust_code, alu_op, done);	 

   assign bus = (read_stack) ? data_stack[data_index] : 16'bz;
   
   
   always @ (posedge clock)
     if (reset) begin
	instruction_index <= 1'b0;
	data_index <= 0;	
	data_stack[0] <= 16'h1337;	
     end
     else begin
	if (write_stack)
	  data_stack[data_index] <= bus;	
	if (micro_instruction_done)
	  instruction_index <= instruction_index + 16'b1;
	case (stack_index_adjust_code)
	  2'b00: data_index <= data_index + 1;
	  2'b01: data_index <= data_index - 1;
	endcase
     end
endmodule
