module micro_cpu(instruction, read_a, read_b, read_stack, read_alu, write_a, write_b, write_stack, stack_index_adjust_code, alu_op, clock, done, reset);
   input clock;
   input reset;   
   input [15:0] instruction;
   reg [15:0] 	micro_instruction_index;   
   reg [31:0] 	micro_instructions [255:0];
   output 	read_a;
   output 	read_b;
   output 	read_stack;
   output 	read_alu;   
   output 	write_a;
   output 	write_b;
   output 	write_stack;
   output [1:0] stack_index_adjust_code;
   output [1:0] alu_op;
   output reg 	done;   
   
   initial
     $readmemh("micro-rom.hex", micro_instructions);

   wire [15:0] current_micro_instruction;   
   assign current_micro_instruction = micro_instructions[micro_instruction_index];

   wire [2:0] source_code;   
   assign source_code = current_micro_instruction[3:1];   
   assign read_a = (source_code == 3'd0);
   assign read_b = (source_code == 3'd1);
   assign read_stack = (source_code == 3'd2);
   assign read_alu = (source_code == 3'd3);   

   wire [2:0] destination_code;
   assign destination_code = current_micro_instruction[6:4];   
   assign write_a = (destination_code == 3'd0);
   assign write_b = (destination_code == 3'd1);
   assign write_stack = (destination_code == 3'd2);

   assign alu_op = current_micro_instruction[8:7];
   
   wire [1:0] stack_index_adjust_code;
   assign stack_index_adjust_code = current_micro_instruction[10:9];

   always @ (posedge clock) begin
      $display("uIP: %d", micro_instruction_index);
      $display("reset: %d", reset);
   end
   
   always @ (posedge clock)
     if (reset || done) begin
	micro_instruction_index <= (instruction[7:0] << 4);
	done <= 1'b0;
     end
     else begin
	micro_instruction_index <= micro_instruction_index + 1;	  
	done <= current_micro_instruction[0];
     end
endmodule
