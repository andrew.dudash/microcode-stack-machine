module test_alu();
   reg [15:0] operand;
   reg [1:0]  operator;
   reg clock;
   reg read;   
   reg reset;
   wire [15:0] result;
   
   alu ALU(operand, operator, result, read, clock, reset);

   initial begin
      operator <= 3'd0;      
      clock <= 1'b0;
      reset <= 1'b1;
      read <= 1'b1;      
      #10 clock <= 1'b1;
      #10 clock <= 1'b0;
      reset <= 1'b0;      
      for (int i = 1; i <= 10; i = i + 1) begin
	 operand <= i;
	 #10 clock <= 1'b1;
	 #10 clock <= 1'b0;
	 $display(result);	 
      end
   end
endmodule
