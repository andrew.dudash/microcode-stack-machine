module test_micro_cpu();
   reg [15:0] instruction;
   wire       read_a, read_b, read_stack, read_alu;
   wire       write_a, write_b, write_stack;
   wire [1:0] stack_index_adjust_code;
   wire [1:0] alu_op;
   reg 	      clock;
   wire       done;
   reg 	      reset;   
   
   micro_cpu MICRO(instruction, read_a, read_b, read_stack, read_alu, write_a, write_b, write_stack, stack_index_adjust_code, alu_op, clock, done, reset);

   initial begin
      clock <= 1'b0;
      reset <= 1'b1;
      instruction <= {8'd0, 8'd0};
      #10 clock <= 1'b1;
      #10 clock <= 1'b0;
      #10 reset <= 1'b0;      

      for (int i = 0; i < 5; i = i + 1) begin
	 $display("Read A: %d, Read B: %d, Read Stack: %d, Read ALU: %d, Write A: %d, Write B: %d, Write Stack: %d, Stack Index Adjust Code: %d, ALU Op: %d, Done %d", read_a, read_b, read_stack, read_alu, write_a, write_b, write_stack, stack_index_adjust_code, alu_op, done);	 
	 #10 clock <= 1'b1;
	 #10 clock <= 1'b0;
      end
   end
endmodule
