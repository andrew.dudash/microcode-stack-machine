module alu(operand, operator, result, read, clock, reset);
   input [15:0] operand;
   input [1:0] 	operator;
   output [15:0] result;
   input 	read;
   input 	clock;
   input 	reset;
   
   reg [15:0] accumulator;
   wire       reset;

   assign result = read ? accumulator : 16'bz;

   localparam ADD = 3'd0;
   localparam SUB = 3'd1;
   localparam LOAD = 3'd2;   

   always @ (posedge clock)
     if (reset)
       accumulator <= 16'd0;
     else   
       case (operator)
	 ADD: accumulator <= accumulator + operand;	 
	 SUB: accumulator <= accumulator - operand;
	 LOAD: accumulator <= operand;	 
	 default: accumulator = 16'dx;
       endcase
endmodule
